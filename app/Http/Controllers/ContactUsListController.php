<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactUsListController extends Controller
{
    public function index()
    {
        $contacts = Contact::all();
        return view('contact-list', compact('contacts', 'contacts'));
    }
    public function show()
    {
        $contacts = Contact::all();
        return view('contact-list', compact('contacts', 'contacts'));
    }

    public function edit($id)
    {
        $contact = Contact::find($id);
        return view('message-edit', compact('contact'));
    }

    public function update(Request $request)
    {
        //code..
    }

    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->delete();
        return redirect('/contact-list')->with('success', 'List deleted successfully');
    }

    public function updateList(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'subject' => 'required',
            'message' => 'required'
        ]);

        $contact = Contact::find($request->get('id'));
        $contact->name = $request->get('name');
        $contact->email = $request->get('email');
        $contact->phone = $request->get('phone');
        $contact->subject = $request->get('subject');
        $contact->message = $request->get('message');
        $contact->save();
        return redirect('/contact-list')->with('success', 'List Updated successfully');
    }
}
