<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function masuk()
    {
        return view('login');
    }
    public function contactUs()
    {
        return view('contact');
    }
}
