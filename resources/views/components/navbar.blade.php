<div class="navbar">
  <div>
      <img class="img-logo" src="https://investree.id/v3/1647494991991/img/inv_logo.55026ac7.png" alt="logo">
  </div>
  <div class="nav-menu">
      <ul>
          <li class="card nav-item">
              <a href="/">Beranda</a>
          </li>
          <li class="card nav-item">
              <a href="#kelebihan">kelebihan</a>
          </li>
          <li class="card nav-item">
              <a href="#product">Produk</a>
          </li>
          <li class="card nav-item">
              <a href="{{url('/contactUs')}}">Hubungi Kami</a>
          </li>
          <li class="card nav-item">
              <a href="{{url('/contact-list')}}">Kotak Pesan</a>
          </li>
      </ul>
  </div>
  <a href="{{url('/masuk')}}" class="button-in">Masuk</a>
</div>