@extends('layout.main')
@section('css')
    <link rel="stylesheet" href="{{ URL::asset('css/contact-page.css') }}">
@endsection

@section('content')
    <div class="container">
        <form action="{{ route('contact.store') }}" method="POST">
            <div >
                @if ($errors->any())
                    <div class="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }} </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            @csrf
            <br>
            <label>Nama</label>
            <input class="input-text" type="text" id="name" name="name" placeholder="Your name..">

            <label>Email</label>
            <input class="input-text" type="email" id="email" name="email" placeholder="Your email.." required>

            <label>No. Telepon</label>
            <input class="input-text" type="text" id="phone" name="phone" placeholder="Your Phone Number.." required>

            <label>Subject</label>
            <input class="input-text" type="text" id="subject" name="subject" placeholder="Write Subject.." required>

            <label>Pesan</label>
            <textarea class="input-text" id="message" name="message" placeholder="Write something.." style="height:200px"
                required></textarea>

            <input type="submit" value="Submit" name="send">

        </form>
    </div>
@endsection
