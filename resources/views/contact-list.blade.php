@extends('layout.main')

@section('css')
    <link rel="stylesheet" href="{{ URL::asset('css/contact-list.css') }}">
@endsection

@section('content')
    <div class="page-title">
        <h2>List Pesan</h2>
    </div>
    <table class="styled-table">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Email</th>
                <th>No. Telepon</th>
                <th>Subject</th>
                <th>Pesan</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($contacts as $contact)
                <tr>
                    <td>{{ $contact->name }}</td>
                    <td>{{ $contact->email }}</td>
                    <td>{{ $contact->phone }}</td>
                    <td>{{ $contact->subject }}</td>
                    <td>{{ $contact->message }}</td>
                    <td>
                        <form action="{{ route('contact-list.destroy', $contact->id) }}" method="POST">
                            <a class="btn btn-primary" href="{{ route('contact-list.edit', $contact->id) }}">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
