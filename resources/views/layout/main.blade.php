<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{ URL::asset('css/landing-page.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @yield('css')
</head>

<body>
    <!-- Navbar -->
    @include('components.navbar')

    <!-- Konten --->
    @yield('content')

    <!-- Footer --->
    <footer class="f-con">
      @include('components.footer')

    </footer>
    <script src="{{ asset('js/landing-page.js') }}"></script>
</body>

</html>
