@extends('layout.main')
@section('content')
    <!-- Konten --->
    <div class="content">
        <!-- Slide Show --->
        <div class="slideshow-container">

            <!-- Full-width images with number and caption text -->
            <div class="mySlides fade">
                <div class="numbertext">1 / 3</div>
                <img src="https://s3-ap-southeast-1.amazonaws.com/loket-production-sg/images/banner/20211113090056_618f1c5868c7e.jpg"
                    style="width:100%">
                <div class="text">Caption Text</div>
            </div>

            <div class="mySlides fade">
                <div class="numbertext">2 / 3</div>
                <img src="https://i0.wp.com/mediaasuransinews.co.id/wp-content/uploads/2021/02/investree-lender.jpg?fit=626%2C368&ssl=1"
                    style="width:100%">
                <div class="text">Caption Two</div>
            </div>

            <div class="mySlides fade">
                <div class="numbertext">3 / 3</div>
                <img src="https://seremonia.id/wp-content/uploads/2021/10/Foto-2-Lomba-reels-di-Instagram-untuk-ulang-tahun-Investree-yang-ke-6-GrowStron6er-768x432.jpg"
                    style="width:100%">
                <div class="text">Caption Three</div>
            </div>

            <!-- Next and previous buttons -->
            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
            <a class="next" onclick="plusSlides(1)">&#10095;</a>
        </div>
        <br>

        <!-- The dots/circles -->
        <div style="text-align:center">
            <span class="dot" onclick="currentSlide(1)"></span>
            <span class="dot" onclick="currentSlide(2)"></span>
            <span class="dot" onclick="currentSlide(3)"></span>
        </div>


        <!-- keuntungan -->
        <div id="kelebihan">
            <h1 class="sub-title">Keuntungan</h1>
            <hr class="line">
            <div class="page-wrapper">
                <div class="row">
                    <div class="kelebihan-item card column">
                        <i class="fa fa-car" style="font-size:35px;"></i>
                        <div class=''>
                            <h2 class="kelebihan-title">LOREM IPSUM</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                labore et
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
                        </div>
                    </div>
                    <div class="kelebihan-item card column">
                        <i class="fa fa-car" style="font-size:35px;"></i>
                        <div class=''>
                            <h2 class="kelebihan-title">LOREM IPSUM</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                labore et
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="kelebihan-item card column">
                        <i class="fa fa-car" style="font-size:35px;"></i>
                        <div class=''>
                            <h2 class="kelebihan-title">LOREM IPSUM</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                labore et
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
                        </div>
                    </div>
                    <div class="kelebihan-item card column">
                        <i class="fa fa-car" style="font-size:35px;"></i>
                        <div class=''>
                            <h2 class="kelebihan-title">LOREM IPSUM</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                labore et
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- product -->
        <div id="product">
            <h1 class="sub-title">Produk Kami</h1>
            <hr class="line">
            <div class="page-wrapper">
                <div class="row">
                    <div class="kelebihan-item card column">
                        <i class="fa fa-car" style="font-size:35px;"></i>
                        <div class=''>
                            <h2 class="kelebihan-title">LOREM IPSUM</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                labore et
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
                        </div>
                    </div>
                    <div class="kelebihan-item card column">
                        <i class="fa fa-car" style="font-size:35px;"></i>
                        <div class=''>
                            <h2 class="kelebihan-title">LOREM IPSUM</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                labore et
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
                        </div>
                    </div>
                    <div class="kelebihan-item card column">
                        <i class="fa fa-car" style="font-size:35px;"></i>
                        <div class=''>
                            <h2 class="kelebihan-title">LOREM IPSUM</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                labore et
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
