@extends('layout.main')
@section('css')
    <link rel="stylesheet" href="{{ URL::asset('css/contact-page.css') }}">
@endsection

@section('content')
    <div class="container">
        <form action="{{ route('update-list') }}" method="POST">
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }} </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            @csrf
            @method('PUT')
            <input hidden name="id" value="{{ $contact->id }}">
            <br>
            <label>Nama</label>
            <input class="input-text" type="text" id="name" name="name" value="{{ $contact->name }}" required>

            <label>Email</label>
            <input class="input-text" type="email" id="email" name="email" value="{{ $contact->email }}" required>

            <label>No. Telepon</label>
            <input class="input-text" type="text" id="phone" name="phone" value="{{ $contact->phone }}" required>

            <label>Subject</label>
            <input class="input-text" type="text" id="subject" name="subject" value="{{ $contact->subject }}" required>

            <label>Pesan</label>
            <textarea class="input-text" id="message" name="message" value="{{ $contact->message }}" style="height:200px"
                required>{{ $contact->message }}</textarea>

            <input type="submit" value="Submit" name="send">

        </form>
    </div>
@endsection
