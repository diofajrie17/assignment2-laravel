<?php

use App\Http\Controllers\ContactUsFormController;
use App\Http\Controllers\ContactUsListController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/masuk', [HomeController::class, 'masuk']);
Route::get('/contactUs', [HomeController::class, 'contactUs']);


Route::get('/contact',[ContactUsFormController::class, 'create']);
Route::post('/contact',[ContactUsFormController::class, 'store'])->name('contact.store');

// Route::get('/contact-list',[ContactUsListController::class, 'index']);
// Route::put('/contact-list/{id}',[ContactUsListController::class, 'edit'])->name('contact.edit');

Route::resource('/contact-list',ContactUsListController::class);
Route::put('/update-list',[ContactUsListController::class, 'updateList'])->name('update-list');